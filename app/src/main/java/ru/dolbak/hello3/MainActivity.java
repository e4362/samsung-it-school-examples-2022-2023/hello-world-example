package ru.dolbak.hello3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    TextView textView;
    Button button;
    Random rand;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.textView);
        button = findViewById(R.id.button2);
        textView.setText("Нажмите на кнопку");
        rand = new Random();
    }

    public void onClick(View view) {
        textView.setText(Integer.toString(rand.nextInt()));
        Toast.makeText(this, "Вы молодец", Toast.LENGTH_SHORT).show();
    }
}